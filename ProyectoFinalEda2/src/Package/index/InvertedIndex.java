package Package.index;
import java.util.ArrayList;
import java.util.HashMap;

import Package.document.Documento;

public class InvertedIndex {

	public static HashMap<String, ArrayList<Documento>> index = new HashMap<String, ArrayList<Documento>>();
	public static ArrayList<Documento> allDocs = new ArrayList<Documento>();

	public ArrayList<Documento> Find(String token) {
		return InvertedIndex.index.get(token);
	}

}
