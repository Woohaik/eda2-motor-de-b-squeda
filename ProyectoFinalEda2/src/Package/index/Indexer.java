package Package.index;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import Package.document.Documento;

import Package.serialize.SerlializeDeserialize;

public class Indexer {
	static Logger logger = Logger.getLogger(Indexer.class);
	public static HashMap<String, Double> IDF = new HashMap<String, Double>();
	public static ArrayList<Documento> documentos = new ArrayList<Documento>();
	public static int[] porcentajeIndexado = { 0, 0 };
	public static boolean indexado = false;
	SerlializeDeserialize serdeser = new SerlializeDeserialize();

	public void ReIndexar() {

	}

	public void Indexar() {
		calcularIDFs();
		calcularIDF_TF();
		ordenarDocumentos();
		limpiarDocumento();
		serdeser.Serializar();
		logger.debug("IDFs calculado");

	}

	// Calcula los IDFs
	public void calcularIDFs() {
		logger.info("Calculando IDFS");
		// Por cada indice
		InvertedIndex.index.forEach((k, v) -> {
			// llama la funcion IDF de cada termino
			logger.debug("funcion IDF llamada para termino: " + k);
			double idf = calcularIDF(k);
			// Va llenando el hashMap de todos los idf para cada token que exista en todos
			// los documentos.
			logger.debug("hashMap de todos los idf para cada token llenado" + idf);
			Indexer.IDF.put(k, idf);

		});

	}

	// Funcion que calcula loas IDF TF por cada token en cada documento
	// individualmente.
	public void calcularIDF_TF() {
		logger.info("Calculando IDF-TF");
		for (Documento documento : documentos) {
			// Por cada documento recupera su frec que contiene el TF y el IDF_TF
			logger.debug("En cada documento se ha recuperado su frec que contiene el TF y el IDF_TF");
			documento.getTermsFreq().forEach((k, v) -> {
				// Recupera el idf que tiene ese token y lo multiplica por el Tf del termino del
				// documento espesifico.
				logger.debug(" idf de los token multiplicado por el Tf del termino");
				double idf = Indexer.IDF.get(k);
				v[1] = idf * v[0];
			});
			Indexer.porcentajeIndexado[1]++;
		}
	}

	// Recibe el termino al que quiera calcularse su idf
	public double calcularIDF(String termino) {

		double idf = (Math.log10((documentos.size() / InvertedIndex.index.get(termino).size())) + 1);
		return idf;
	}

	public void ordenarDocumentos() {
		InvertedIndex.index.forEach((k, v) -> {
			boolean ordenado = false;
			while (!ordenado) {
				boolean cambios = false;
				for (int i = 0; i < (v.size() - 1); i++) {
					Documento anterior = v.get(i);
					Documento siguiente = v.get(i + 1);
					if ((anterior.getIDF_TF(k)) < (siguiente.getIDF_TF(k))) {
						v.set(i, siguiente);
						v.set(i + 1, anterior);
						cambios = true;
					}
				}
				if (cambios == false) {
					ordenado = true;
					logger.debug("Documentos con token " + k + " ordenandos");
				}

			}
		});
		logger.info("Todos los documentos ordenados");
		indexado = true;
		logger.debug("indexado completado");
		documentos.clear();

	}

	public void limpiarDocumento() {
		logger.debug("Documentos limpiados de indice Invertido");
		InvertedIndex.index.forEach((k, v) -> {
			for (int i = 0; i < (v.size()); i++) {
				v.get(i).dropTF();
				;
			}

		});
		Indexer.porcentajeIndexado[0] = 0;
		Indexer.porcentajeIndexado[1] = 0;

	}

}
