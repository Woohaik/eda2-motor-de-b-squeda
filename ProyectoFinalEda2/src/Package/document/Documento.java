package Package.document;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import Package.index.Indexer;
import Package.index.InvertedIndex;

public class Documento implements Serializable {
	static Logger logger = Logger.getLogger(Documento.class);

	private static final long serialVersionUID = -4444737266667249536L;
	private String path;
	private File docFile;
	private String docName;
	private String[] contenido;
	private HashMap<String, Double[]> termFreq;

	public Documento(String[] contenido, File docFile) {
		logger.info("Se ha agregado el documento " + docFile.getName());
		Indexer.porcentajeIndexado[1]++;

		Indexer.porcentajeIndexado[1]++;

		this.docFile = docFile;

		this.path = docFile.getPath();
		this.docName = docFile.getName();
		this.contenido = contenido;
		termFreq = new HashMap<String, Double[]>();
		calcLocalTF();
		logger.info("Se ha terminado de calcular el TF para cada token del documento " + docFile.getName());
		Indexer.porcentajeIndexado[1]++;

		Indexer.porcentajeIndexado[1]++;

		Indexer.porcentajeIndexado[1]++;

		InvertedIndex.allDocs.add(this);

		this.contenido = null;
		logger.debug("path del archivo: " + docFile.getPath());

	}

	public Documento(String[] contenido) {

		this.contenido = contenido;
		termFreq = new HashMap<String, Double[]>();
		calcLocalTF();
	}

	public void dropTF() {
		this.termFreq.clear();
	}

	public String[] getContenido() {
		return this.contenido;
	}

	public File getFile() {
		return this.docFile;
	}

	public String getName() {
		return this.docName;
	}

	public String getPath() {
		return this.path;
	}

	public double getIDF_TF(String token) {
		return this.termFreq.get(token)[1];
	}

	public HashMap<String, Double[]> getTermsFreq() {
		return this.termFreq;
	}

	public Double getTF(String token) {
		return termFreq.get(token)[0];
	}

	private void calcLocalTF() {
		for (String token : contenido) {
			// Si el token aun no esta lo agrega iniciando el contador con 1 y el idf*tf de
			// momento a 0
			if (!(termFreq.containsKey(token))) {
				Double[] inicializacionToken = { 1.0, 0.0 };
				termFreq.put(token, inicializacionToken);
				logger.debug("token inicializado: " + inicializacionToken);

				// Agregando token a la lista principal de token si este aun no esta
				if (!(InvertedIndex.index.containsKey(token))) {
					ArrayList<Documento> documentosConToken = new ArrayList<Documento>();
					// De no estar lo agrega y como value pone este documento a la lista de ese
					// token
					documentosConToken.add(this);
					InvertedIndex.index.put(token, documentosConToken);
					logger.debug("agrega value en la lista del token: " + documentosConToken.add(this));

				} else {
					// Si ese token ya esta en la lista principal solo agrega este documento a la
					// lista.
					ArrayList<Documento> listaAnterior = InvertedIndex.index.get(token);
					listaAnterior.add(this);
					InvertedIndex.index.replace(token, listaAnterior);
					logger.debug("agrega el token a" + InvertedIndex.index.get(token) + "la lista anterior");
				}

				// Si ya esta el token lo busca en el map y le suma uno al valor
			} else {
				double valorAnterior = termFreq.get(token)[0];
				valorAnterior++;
				Double[] sumandoTF = { valorAnterior, 0.0 };
				termFreq.replace(token, sumandoTF);
				logger.debug("ya esta el token y lo busca en el map");
			}

		}

	}

}
