package Package.document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.OfficeParser;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;
import Package.index.Indexer;
import Package.pakage.Progress;

public class DocumentParser {
	static Logger logger = Logger.getLogger(DocumentParser.class);

	public static ArrayList<File> files = new ArrayList<File>();

	public void addNewFile(File newFile) {
		logger.info("Agregado un nuevo archivo a lista a indexar  " + newFile.getName());
		logger.debug("Path del archivo a indexar  " + newFile.getPath());
		DocumentParser.files.add(newFile);
	}

	public void clearChargedFiles() {
		logger.info("Se ha limpiado la lista a indexar");
		logger.debug("Lista ahora con: " + files.size() + " elementos");
		DocumentParser.files.clear();

	}

	public ArrayList<File> getChargedFiles() {
		return DocumentParser.files;
	}

	public Vector<String> getFilesNames() {
		Vector<String> filesNames = new Vector<String>();
		for (File individual : files) {
			filesNames.add(individual.getName());

		}

		return filesNames;
	}

	public void indexarDocs() {

		if (!files.isEmpty()) {
			logger.info("Comenzando indexacion");
			almacenarDocumentos();
			Indexer indexer = new Indexer();
			indexer.Indexar();
			clearChargedFiles();
			logger.info("Limpiar archivos en lista de espera de indexacion");
		} else {
			logger.error("No se puede indexar una lista sin documentos");
		}

	}

	public String parseDocument(File file) {
		String fileXtension = fileExtension(file);
		String documentParsed = "";

		if (fileXtension.equals("doc")) {
			documentParsed = parserDOC(file);

		} else if (fileXtension.equals("txt")) {
			documentParsed = parserTXT(file);

		} else if (fileXtension.equals("xls")) {
			documentParsed = parserXLS(file);

		} else if (fileXtension.equals("pdf")) {
			documentParsed = parserPDF(file);

		} else {
			logger.error("No se puede parsear esta extension");
		}

		return documentParsed.toLowerCase();

	}

	public void almacenarDocumentos() {

		Indexer.porcentajeIndexado[0] = files.size();

		Progress pBar = new Progress();

		pBar.start();

		for (File fileIndividual : files) {
			String textoDocumento = parseDocument(fileIndividual);

			if (!textoDocumento.equals("")) {
				Indexer.documentos.add(new Documento(slipDocument(parseDocument(fileIndividual)), fileIndividual));

			} else {
				logger.error("No se va a indexar un documento vacio " + fileIndividual.getPath() + " esta vacio");
				Indexer.porcentajeIndexado[0]--;
			}

		}
	}

	// funcion que separa cada palabra del documento.
	public String[] slipDocument(String documento) {
		String[] arr = documento.replaceAll("(\n|\r|-|\\+|/|=)", " ").split("[,\\:\" ?	.@]+");
		return arr;
	}

	private String parserPDF(File file) {
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = null;
		try {
			inputstream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}
		ParseContext pcontext = new ParseContext();
		// parsing the document using PDF parser
		PDFParser pdfparser = new PDFParser();
		try {
			pdfparser.parse(inputstream, handler, metadata, pcontext);
		} catch (IOException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (TikaException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}

		// retorna el contenido del pdf
		return handler.toString();
	}

	private String parserTXT(File file) {
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = null;
		try {
			inputstream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ParseContext pcontext = new ParseContext();
		// txt parser
		TXTParser TexTParser = new TXTParser();
		try {
			TexTParser.parse(inputstream, handler, metadata, pcontext);
		} catch (IOException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (TikaException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}
		return handler.toString();
	}

	private String parserDOC(File file) {
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = null;
		try {
			inputstream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}
		ParseContext pcontext = new ParseContext();
		// txt parser
		OfficeParser wordExtractor = new OfficeParser();
		try {
			wordExtractor.parse(inputstream, handler, metadata, pcontext);
		} catch (IOException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		} catch (TikaException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}
		return handler.toString();
	}

	private String parserXLS(File file) {
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = null;
		try {
			inputstream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			logger.error("Exepcion " + e);
			e.printStackTrace();
		}
		ParseContext pcontext = new ParseContext();

		// OOXml parser
		OOXMLParser msofficeparser = new OOXMLParser();
		try {
			msofficeparser.parse(inputstream, handler, metadata, pcontext);
		} catch (IOException e) {
			logger.error("Exepcion " + e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("Exepcion " + e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TikaException e) {
			logger.error("Exepcion " + e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// retorna el contenido del ms-doc
		return handler.toString();

	}

	public String fileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		} else {
			return "";
		}

	}

}
