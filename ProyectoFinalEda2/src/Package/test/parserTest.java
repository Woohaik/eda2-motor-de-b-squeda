package Package.test;

import java.io.File;
import org.junit.Assert;
import org.junit.Test;
import Package.document.DocumentParser;

public class parserTest {

	// VERIFICA LAS EXTENSIONES DE LOS ARCHIVOS

	@Test // test para extension .pdf
	public void fileExtensionPdfTest() {
		DocumentParser doc = new DocumentParser();
		File pdfFile = new File("prueba/prueba.pdf");

		String expected = "pdf";
		String recived = doc.fileExtension(pdfFile);
		Assert.assertEquals(expected, recived);

	}

	@Test // test para extension .txt
	public void fileExtensionTxtTest() {
		DocumentParser doc = new DocumentParser();
		File txtFile = new File("prueba/prueba3.txt");

		String expected = "txt";
		String recived = doc.fileExtension(txtFile);
		Assert.assertEquals(expected, recived);

	}

	@Test // test para extension .doc
	public void fileExtensionDocTest() {
		DocumentParser doc = new DocumentParser();
		File docFile = new File("prueba/pruebadoc.doc");

		String expected = "doc";
		String recived = doc.fileExtension(docFile);
		Assert.assertEquals(expected, recived);

	}

	@Test // test para extension .xls
	public void fileExtensionXlsTest() {
		DocumentParser doc = new DocumentParser();
		File xlsFile = new File("prueba/example.xls");

		String expected = "xls";
		String recived = doc.fileExtension(xlsFile);
		Assert.assertEquals(expected, recived);

	}
	// VERIFICA SI ESTA PARSEANDO LOS ARCHIVOS

	@Test // test para el parser de txt
	public void parserTxtTest() {
		DocumentParser doc = new DocumentParser();
		File txtFile = new File("prueba/prueba2.txt");

		String recived = doc.parseDocument(txtFile);
		Assert.assertTrue(recived.contains("banana"));

	}

	@Test // test para el parser de pdf
	public void parserPdfTest() {
		DocumentParser doc = new DocumentParser();
		File pdfFile = new File("prueba/prueba.pdf");

		String expected = "dias";
		String recived = doc.parseDocument(pdfFile);
		Assert.assertTrue(recived.contains(expected));

	}

	@Test // test para el parser de doc
	public void parserDocTest() {
		DocumentParser doc = new DocumentParser();
		File docFile = new File("prueba/pruebadoc.doc");

		String expected = "doc";
		String recived = doc.parseDocument(docFile);
		Assert.assertTrue(recived.contains(expected));

	}

	@Test // test para el parser de xls
	public void parserXlsTest() {
		DocumentParser doc = new DocumentParser();
		File xlsFile = new File("prueba/example.xls");

		String recived = doc.parseDocument(xlsFile);
		Assert.assertTrue(recived.contains("funiber"));

	}
}
