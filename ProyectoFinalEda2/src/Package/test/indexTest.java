package Package.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import Package.document.DocumentParser;
import Package.document.Documento;
import Package.index.Indexer;
import Package.index.InvertedIndex;

public class indexTest {
	DocumentParser dp = new DocumentParser();

	@Test // test para extension .pdf
	public void testTF() {
		File txtFile = new File("prueba/prueba3.txt");
		File docFile = new File("prueba/pruebadoc.doc");
		String contenidotxt = dp.parseDocument(txtFile);
		String contenidoc = dp.parseDocument(docFile);
		Indexer.documentos.add(new Documento(dp.slipDocument(contenidotxt), txtFile));
		Indexer.documentos.add(new Documento(dp.slipDocument(contenidoc), docFile));
		// Se espera que el TF de la palabra limpiar sea de 3
		Assert.assertTrue(Indexer.documentos.get(0).getTF("limpiar") == 3);
		Assert.assertTrue(Indexer.documentos.get(1).getTF("doc") == 2);

		InvertedIndex.index.clear();
		Indexer.documentos.clear();

	}

	public void testInvertedIndex() {
		File txtFile = new File("prueba/prueba3.txt");
		File docFile = new File("prueba/pruebadoc.doc");
		String contenidotxt = dp.parseDocument(txtFile);
		String contenidoc = dp.parseDocument(docFile);
		Indexer.documentos.add(new Documento(dp.slipDocument(contenidotxt), txtFile));
		Indexer.documentos.add(new Documento(dp.slipDocument(contenidoc), docFile));
		Indexer indexe = new Indexer();
		indexe.calcularIDFs();
		indexe.calcularIDF_TF();
		// comprueba que este guardado el token limpiar
		Assert.assertTrue(InvertedIndex.index.containsKey("limpiar"));
		// comprueba que este guardado el token limpiar
		Assert.assertTrue(InvertedIndex.index.containsKey("no"));
		// comprueba que para el token "doc" el primer elemento es el archivo con nombre
		// pruebadoc.doc
		Assert.assertTrue(InvertedIndex.index.get("doc").get(0).getName() == "pruebadoc.doc");
		InvertedIndex.index.clear();
		Indexer.documentos.clear();

	}

}
