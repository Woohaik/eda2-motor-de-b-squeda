package Package.serialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Package.document.Documento;
import Package.pakage.Progress;

public class SerializableIndex implements Serializable {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	private HashMap<String, ArrayList<Documento>> sIndex = new HashMap<String, ArrayList<Documento>>();
	private ArrayList<Documento> sAllDocs = new ArrayList<Documento>();

	public SerializableIndex(HashMap<String, ArrayList<Documento>> indiceASerializar, ArrayList<Documento> allDcs) {
		this.sIndex = indiceASerializar;
		this.sAllDocs = allDcs;

	}



	public HashMap<String, ArrayList<Documento>> getIndex() {
		return this.sIndex;
	}

	public ArrayList<Documento> getAllDocs() {
		return this.sAllDocs;
	}

}
