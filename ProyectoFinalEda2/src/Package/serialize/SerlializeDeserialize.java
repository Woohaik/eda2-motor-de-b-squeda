package Package.serialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import Package.index.*;

public class SerlializeDeserialize {

	private File indexFile = new File("indice.txt");

	public Boolean indexExist() {
		return indexFile.exists();
	}

	public void Serializar() {
		SerializableIndex sI = new SerializableIndex(InvertedIndex.index, InvertedIndex.allDocs);
		try {
			FileOutputStream fos = new FileOutputStream(indexFile);
			ObjectOutputStream sos = new ObjectOutputStream(fos);

			sos.writeObject(sI);
			sos.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public void borrarSerializacion() {
		indexFile.delete();
	}

	public void Deserializar() {
		SerializableIndex indiceLeido;
		try {
			FileInputStream fis = new FileInputStream(indexFile);
			ObjectInputStream sis = new ObjectInputStream(fis);
			indiceLeido = (SerializableIndex) sis.readObject();
			InvertedIndex.index = indiceLeido.getIndex();
			InvertedIndex.allDocs = indiceLeido.getAllDocs();

			/*
			 * indiceLeido.getIndex().forEach((k, v) -> { System.out.println(k); });
			 */

			sis.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

	}

}
