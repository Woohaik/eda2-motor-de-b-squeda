package Package.pakage;

import java.awt.Color;
import org.apache.log4j.Logger;

import Package.index.Indexer;

public class Progress extends Thread {
	static Logger logger = Logger.getLogger(Progress.class);

	@Override
	public void run() {
		logger.info("Comenzando progreso Indexacion");
		Indexer.indexado = false;

		while (!Indexer.indexado) {
			VentanaBuscar.jProgressBar.setValue(calcPer());
		}
		logger.info("Indexacion terminada");

		try {
			Thread.sleep(1000);

		} catch (Exception e) {
			logger.error("Exepcion: "  + e);
		}
		VentanaBuscar.jProgressBar.setValue(0);
		VentanaBuscar.jProgressBar.setBackground(new Color(0, 0, 0, 0));

	}

	private Integer calcPer() {
		int maximo = 100;
		int total = Indexer.porcentajeIndexado[0];
		int indexado = Indexer.porcentajeIndexado[1];
		return (indexado * maximo) / (total * 6);

	}
}
